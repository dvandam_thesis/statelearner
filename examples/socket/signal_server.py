import socket
import requests
import psycopg2
import random
import string
import base64

# LearnLib
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(("localhost", 6666))
s.listen(1)

(client, address) = s.accept()

# Parameters
url = 'https://localhost'
header_get = {
    "Authorization": "Basic KzMxNjExMTExMTExOk1SMXNTSjZSY2VIOWtvQm0zalpZMldXYQ==",
    "X-Signal-Agent": "OWA",
    "Accept-Encoding": "gzip",
    "User-Agent": "okhttp/3.9.0"
}
header_put = {
    "Authorization": "Basic KzMxNjExMTExMTExOk1SMXNTSjZSY2VIOWtvQm0zalpZMldXYQ==",
    "X-Signal-Agent": "OWA",
    "Accept-Encoding": "gzip",
    "User-Agent": "okhttp/3.9.0",
    "Content-Type": "application/json; charset=utf-8"
}
verification_code = '000000'
phone_number = '+31611111111'

def clean_database():
    try:
        connection = psycopg2.connect(
           user = "postgres",
           password = "postgres",
           host = "127.0.0.1",
           port = "5432",
           database = "accountsdb")
        cursor = connection.cursor()
        cursor.execute("DELETE FROM accounts;")
        connection.commit()
        cursor.execute("DELETE FROM keys;")
        connection.commit()
    except (Exception, psycopg2.Error) as error:
        print ("Error while connecting to PostgreSQL", error)
    finally:
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")

def update_headers():
    global phone_number
    phone_number = generate_phone_number()
    token = "Basic " + generate_basic_auth_token(phone_number)
    header_get["Authorization"] = token
    header_put["Authorization"] = token

def generate_phone_number():
    return '+' + str(random.randint(10000000000,99999999999))

def generate_basic_auth_token(phone_number):
    password = ''.join([random.choice(string.ascii_letters + string.digits)
        for n in xrange(16)])
    return base64.b64encode(phone_number + ':' + password)


# main loop
while 1:
    cmd = client.recv(1024).strip()

    if cmd == "":
        break

    if cmd == "RESET":
        clean_database()
        update_headers()
        response = "DONE"
    elif cmd == "REQUEST_VERIFICATION_CODE":
        r = requests.get(
            url + '/v1/accounts/sms/code/' + phone_number,
            headers=header_get,
            verify=False)
        verification_code=r.content
        response = str(r.status_code)
    elif cmd == "CONFIRM_VERIFICATION_CODE":
        payload={
            "fetchesMessages":'false',
            "pin":'null',
            "registrationId":'11092',
            "signalingKey":"/aDYzPPiXTxoQ1juBeIF4T2ru/3C5nB8X0MsV7/kahKhV20P6aaugCx4/xSzN+4jfwc9Tg==",
            "video":'true',
            "voice":'true'
        }
        r = requests.put(
            url + '/v1/accounts/code/' + verification_code,
            headers=header_put,
            json=payload,
            verify=False)
        response = str(r.status_code)
    elif cmd == "REGISTER_KEYS":
        payload={
            "identityKey":"BaUQEan9RhY/0ina2Ey8GTbAICWCLifMmwphLpXouYsC",
            "preKeys":
            [
                {"keyId":11494154,"publicKey":"BeKIi8JcPDJtUEj5E9o2iILZVOvxLHp+/2pQgUmrvT0G"},
                {"keyId":11494155,"publicKey":"BXLGjH/+kCMfoZQWkcxeplN0S3oqzel/7fXkzhO+04lN"},
                {"keyId":11494156,"publicKey":"BaaybIOIkJucecjAL/WJb59P77KvCtHddSpYnSvaEDId"},
                {"keyId":11494157,"publicKey":"Bftx46dgaWD/BbLCJiHRM8mVz6kcT1gSXvz2ZMHjkHhM"},
                {"keyId":11494158,"publicKey":"BZLaWgjbeKmPAvsNuYcLM+jlJpj9eROMPH1L5UF+D4sW"},
            ],
            "signedPreKey":
            {
                "keyId":15273084,
                "publicKey":"BXOhm+95qNypSut1WwiGk1ZGH8oHaZIqFIBqXlYSbyxD",
                "signature":"MszpBIcvcpEEU0iBpM/t//xLmTabIaBxWUMySnLPlxA1f6ACWKX89T1je/KJQWZ7KY57dX7QJOgkZoqYQzqIAw"
            }
        }
        r = requests.put(
            url + '/v2/keys',
            headers=header_put,
            json=payload,
            verify=False)
        response = str(r.status_code)
    elif cmd == "RETRIEVE_CONTACTS":
        payload={
            "contacts":
            [
                "tOLmkEWlS/Ogfg",
                "Mnz3hr+qjflK0A",
                "131SHxHvHqWZgg"
            ]
        }
        r = requests.put(
            url + '/v1/directory/tokens',
            headers=header_put,
            json=payload,
            verify=False)
        response = str(r.status_code)
    elif cmd == "RETRIEVE_PREKEY_BUNDLE":
        r = requests.get(
            url + '/v2/keys/+31611945665/*',
            headers=header_put,
            json=payload,
            verify=False)
        response = str(r.status_code)

    client.sendall(response + "\n")
