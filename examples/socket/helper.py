import psycopg2
import random
import base64
import urllib3
import requests
import string
import json

# We use a self-signed certificate for Signal
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def HEADER_BASIC():
    return {
        "X-Signal-Agent": "OWA",
        "Accept-Encoding": "gzip",
        "User-Agent": "okhttp/3.9.0"
    }


def clean_database():
    try:
        connection = psycopg2.connect(
           user = "postgres",
           password = "postgres",
           host = "127.0.0.1",
           port = "5432",
           database = "accountsdb")
        cursor = connection.cursor()
        cursor.execute("DELETE FROM accounts;")
        connection.commit()
        cursor.execute("DELETE FROM keys;")
        connection.commit()
    except (Exception, psycopg2.Error) as error:
        print ("Error while connecting to PostgreSQL", error)
    finally:
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")


def generate_phone_number():
    return '+' + str(random.randint(10000000000,99999999999))


def generate_basic_auth_token(phone_number):
    password = ''.join([random.choice(string.ascii_letters + string.digits)
        for n in range(16)])
    return "Basic " + base64.b64encode(phone_number + ':' + password)


def get_request(url, param, token):
    header = HEADER_BASIC()
    header["Authorization"] = token
    return requests.get(url + param, headers=header, verify=False)


def put_request(url, param, payload, token):
    header = HEADER_BASIC()
    header["Authorization"] = token
    header["Content-Type"]  = "application/json; charset=utf-8"
    return requests.put(url + param, headers=header, json=payload, verify=False)


def delete_request(url, param, token):
    header = HEADER_BASIC()
    header["Authorization"] = token
    header["Content-Type"]  = "application/json; charset=utf-8"
    return requests.delete(url + param, headers=header, verify=False)
