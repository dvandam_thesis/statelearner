from __future__ import print_function
from payloads import *
from helper import *

import socket
import json

def get_phone_number(device):
    if device == 'A':
        return phone_number_A
    elif device == 'B':
        return phone_number_B
    elif device == 'C':
        return phone_number_C
    else:
        return phone_number_Z


def get_token(device):
    if device == 'A':
        return token_A
    elif device == 'B':
        return token_B
    elif device == 'C':
        return token_C
    else:
        return token_Z


def register(device):
    print("register " + device + ": ", end='')

    # request code
    r = get_request(
            url,
            '/v1/accounts/sms/code/' + get_phone_number(device),
            device)
    verification_code = r.content
    if r.status_code != 200:
        print(str(r.status_code))
        return str(r.status_code)

    # confirm code
    r = put_request(
            url,
            '/v1/accounts/code/' + verification_code,
            PAYLOAD_CONFIRM_CODE(),
            get_token(device))
    if r.status_code != 204:
        print(str(r.status_code))
        return str(r.status_code)

    # register GCM
    r = put_request(
            url,
            '/v1/accounts/gcm',
            PAYLOAD_REGISTER_GCM(),
            get_token(device))
    if r.status_code != 204:
        print(str(r.status_code))
        return str(r.status_code)

    # register keys
    r = put_request(
            url,
            '/v2/keys',
            PAYLOAD_REGISTER_KEYS(),
            get_token(device))
    print(str(r.status_code))
    return str(r.status_code)


def add_device(device):
    print("add device to " + device, end=": ")

    # request code
    r = get_request(
            url,
            '/v1/devices/provisioning/code',
            get_token(device))
    if r.status_code != 200:
        return str(r.status_code)
    content = json.loads(r.content)
    verification_code = content["verificationCode"]

    # confirm code
    r = put_request(
            url,
            '/v1/devices/' + verification_code,
            PAYLOAD_ADD_DEVICE(),
            token_A2)
    if r.status_code != 200:
        return str(r.status_code)

    update_database(get_token(device))
    return str(r.status_code)


def message_to(device, payload):
    r = put_request(
            url,
            '/v1/messages/' + get_phone_number(device),
            payload,
            get_token('Z'))
    if (payload == PAYLOAD_MULTI_DEVICE_MESSAGE()):
        print("multi", end=' ')
    print("message to " + device + ": " + str(r.status_code) + '  ' + r.content)
    return str(r.status_code)


def delete_gcm(device):
    r = delete_request(url, '/v1/accounts/gcm', get_token(device))
    print("delete gcm " + device + ": " + str(r.status_code) + '  ' + r.content)
    return str(r.status_code)


# LearnLib
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(("localhost", 6666))
s.listen(1)
(client, address) = s.accept()

# Signal Server
url = 'https://localhost'

# Users
phone_number_A = '+31611111111'
phone_number_B = '+31611111112'
phone_number_C = '+31611111113'
phone_number_Z = '+31612345678' # Sending device, not part of the model
token_A = generate_basic_auth_token(phone_number_A)
token_A2= generate_basic_auth_token(phone_number_A)
token_B = generate_basic_auth_token(phone_number_B)
token_C = generate_basic_auth_token(phone_number_C)
token_Z = generate_basic_auth_token(phone_number_Z)

# main loop
while 1:
    cmd = client.recv(1024).strip()

    if cmd == "":
        break

    if cmd == "RESET":
        print("\n--------\nRESET")
        clean_database()
        delete_gcm('A')
        delete_gcm('B')
        delete_gcm( 'C')
        register('Z')
        print("\n")
        response = "DONE"
    elif cmd == "REGISTER_A":
        response = register('A')
    elif cmd == "REGISTER_B":
        response = register('B')
    elif cmd == "REGISTER_C":
        response = register('C')
    elif cmd == "ADD_DEVICE_TO_A":
        response = add_device('A')
    elif cmd == "ADD_DEVICE_TO_B":
        response = add_device('B')
    elif cmd == "ADD_DEVICE_TO_C":
        response = add_device('C')
    elif cmd == "MESSAGE_TO_A":
        response = message_to('A', PAYLOAD_MESSAGE())
    elif cmd == "MESSAGE_TO_B":
        response = message_to('B', PAYLOAD_MESSAGE())
    elif cmd == "MESSAGE_TO_C":
        response = message_to('C', PAYLOAD_MESSAGE())
    elif cmd == "MULTI_MESSAGE_TO_A":
        response = message_to('A', PAYLOAD_MULTI_DEVICE_MESSAGE())
    elif cmd == "MULTI_MESSAGE_TO_B":
        response = message_to('B', PAYLOAD_MULTI_DEVICE_MESSAGE())
    elif cmd == "MULTI_MESSAGE_TO_C":
        response = message_to('C', PAYLOAD_MULTI_DEVICE_MESSAGE())
    elif cmd == "DELETE_A":
        response = delete_gcm('A')
    elif cmd == "DELETE_B":
        response = delete_gcm('B')
    elif cmd == "DELETE_C":
        response = delete_gcm('C')
    else:
        print("Command '" + cmd + "' unknown, exiting...")
        exit(1)

    client.sendall(response + "\n")
