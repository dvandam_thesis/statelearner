# Define the variables as function so that the values cannot be changed

def PAYLOAD_CONFIRM_CODE():
    return {
        "fetchesMessages":'false',
        "pin":'null',
        "registrationId":'11092',
        "signalingKey":"/aDYzPPiXTxoQ1juBeIF4T2ru/3C5nB8X0MsV7/kahKhV20P6aaugCx4/xSzN+4jfwc9Tg==",
        "video":'true',
        "voice":'true'
    }


def PAYLOAD_REGISTER_GCM():
    return {"gcmRegistrationId": "gcmcode"}


def PAYLOAD_REGISTER_KEYS():
    return {
        "identityKey":"BaUQEan9RhY/0ina2Ey8GTbAICWCLifMmwphLpXouYsC",
        "preKeys":
        [
            {"keyId":11494154,"publicKey":"BeKIi8JcPDJtUEj5E9o2iILZVOvxLHp+/2pQgUmrvT0G"},
            {"keyId":11494155,"publicKey":"BXLGjH/+kCMfoZQWkcxeplN0S3oqzel/7fXkzhO+04lN"},
            {"keyId":11494156,"publicKey":"BaaybIOIkJucecjAL/WJb59P77KvCtHddSpYnSvaEDId"},
            {"keyId":11494157,"publicKey":"Bftx46dgaWD/BbLCJiHRM8mVz6kcT1gSXvz2ZMHjkHhM"},
            {"keyId":11494158,"publicKey":"BZLaWgjbeKmPAvsNuYcLM+jlJpj9eROMPH1L5UF+D4sW"},
        ],
        "signedPreKey":
        {
            "keyId":15273084,
            "publicKey":"BXOhm+95qNypSut1WwiGk1ZGH8oHaZIqFIBqXlYSbyxD",
            "signature":"MszpBIcvcpEEU0iBpM/t//xLmTabIaBxWUMySnLPlxA1f6ACWKX89T1je/KJQWZ7KY57dX7QJOgkZoqYQzqIAw"
        }
    }


def PAYLOAD_REGISTER_KEYS_2():
    return {
        "deviceId":2,
        "identityKey":"AaUQEan9RhY/0ina2Ey8GTbAICWCLifMmwphLpXouYsC",
        "preKeys":
        [
            {"keyId":21494154,"publicKey":"AeKIi8JcPDJtUEj5E9o2iILZVOvxLHp+/2pQgUmrvT0G"},
            {"keyId":21494155,"publicKey":"AXLGjH/+kCMfoZQWkcxeplN0S3oqzel/7fXkzhO+04lN"},
            {"keyId":21494156,"publicKey":"AaaybIOIkJucecjAL/WJb59P77KvCtHddSpYnSvaEDId"},
            {"keyId":21494157,"publicKey":"Aftx46dgaWD/BbLCJiHRM8mVz6kcT1gSXvz2ZMHjkHhM"},
            {"keyId":21494158,"publicKey":"AZLaWgjbeKmPAvsNuYcLM+jlJpj9eROMPH1L5UF+D4sW"},
        ],
        "signedPreKey":
        {
            "keyId":15273084,
            "publicKey":"AXOhm+95qNypSut1WwiGk1ZGH8oHaZIqFIBqXlYSbyxD",
            "signature":"AszpBIcvcpEEU0iBpM/t//xLmTabIaBxWUMySnLPlxA1f6ACWKX89T1je/KJQWZ7KY57dX7QJOgkZoqYQzqIAw"
        }
    }

def PAYLOAD_ADD_DEVICE():
    return {
        "name":"aaa",
        "fetchesMessages":'false',
        "pin":'null',
        "registrationId":11093,
        "signalingKey":"/aDYzPPiXTxoQ1juBeIF4T2ru/3C5nB8X0MsV7/kahKhV20P6aaugCx4/xSzN+4jfwc9Tg==",
        "video":'true',
        "voice":'true',
        "gcmId": "gcmcode"
    }


def PAYLOAD_MESSAGE():
    return {
        "messages" : [{
            "type" : 1,
            "destinationDeviceId" : 1,
            "body" : "aaaaa",
            "timestamp" : 12345
        }]
    }

def PAYLOAD_MULTI_DEVICE_MESSAGE():
    return {
        "messages" : [{
            "type" : 1,
            "destinationDeviceId" : 1,
            "destinationRegistrationId" : 11092,
            "body" : "aaaaa",
            "timestamp" : 12345
        },
        {
            "type" : 1,
            "destinationDeviceId" : 2,
            "destinationRegistrationId" : 11093,
            "body" : "aaaaa",
            "timestamp" : 12345
        }]
    }
